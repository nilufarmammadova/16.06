--SET OPERATORS
--UNION
SELECT
    employee_id
FROM
    employees
UNION
SELECT
    department_id
FROM
    departments;

--UNION ALL
SELECT
    department_name
FROM
    departments
UNION ALL
SELECT
    first_name
FROM
    employees;

--INTERSECT
SELECT
    location_id
FROM
    departments
INTERSECT
SELECT
    location_id
FROM
    locations;

--MINUS
SELECT
    department_id
FROM
    employees
MINUS
SELECT
    department_id
FROM
    departments;
