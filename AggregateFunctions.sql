--AGGREGATE FUNCTIONS
--COUNT, SUM, MIN, MAX, AVG, MEDIAN
SELECT
    COUNT(*) AS total_rows
FROM
    employees;

--With group by
SELECT
    department_id,
    COUNT(employee_id)
FROM
    employees
GROUP BY
    department_id;

--Without group by
SELECT
    SUM(salary)
FROM
    employees;

--With group by
SELECT
    job_id,
    SUM(salary)
FROM
    employees
GROUP BY
    job_id;

--Without group by
SELECT
    min(salary)
FROM
    employees;

--With group by
SELECT
    department_id,
    min(salary)
FROM
    employees
GROUP BY
    department_id;

--Without group by
SELECT
    MAX(salary)
FROM
    employees;

--With group by
SELECT
    manager_id,
    MAX(salary)
FROM
    employees
GROUP BY
    manager_id;

--Without group by
SELECT
    AVG(salary)
FROM
    employees;

--With group by
SELECT
    hire_date,
    AVG(salary)
FROM
    employees
GROUP BY
    hire_date;

--Without group by
SELECT
    median(salary)
FROM
    employees;
