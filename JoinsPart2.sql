--JOINS
--Left
SELECT
    first_name,
    department_name
FROM
    employees   e
    LEFT JOIN departments d ON e.department_id = d.department_id;

--Right
SELECT
    last_name,
    department_name
FROM
    employees   e
    RIGHT JOIN departments d ON e.department_id = d.department_id;

--Full
SELECT
    d.department_id,
    salary
FROM
    employees   e
    FULL JOIN departments d ON e.department_id = d.department_id;
    
--Cross
SELECT
    middle_name
FROM
    employees cross
JOIN DEPARTMENTS ;

--Non-equi
SELECT
    department_name,
    l.location_id
FROM
         departments d
    INNER JOIN locations l ON d.location_id <= l.location_id;
