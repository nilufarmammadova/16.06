--SUBQUERIES
--Find employees who have a salary above the avregae salary of all employees. 
SELECT
    *
FROM
    employees
WHERE
    salary > (
        SELECT
            AVG(salary)
        FROM
            employees
    );

--Identify the employees who work in a department with more than 10 employees.
SELECT
    *
FROM
    employees where department_id in (select department_id
from employees 
group by department_id
having count(employee_id) > 10);

--Find employees who have been with the company longer than the average tenure. 
SELECT
    *
FROM
    employees
WHERE
    months_between(sysdate, hire_date) > (
        SELECT
            AVG(months_between(sysdate, hire_date)) AS average_tenure
        FROM
            employees
    );
