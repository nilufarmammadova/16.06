--CONVERSION FUNCTIONS
--TO_CHAR, TO_DATE, TO_NUMBER, CASE, NVL, DECODE, NULLIF
--TO_CHAR
SELECT to_char (hire_date) FROM employees;

--TO_DATE
SELECT to_date (hire_date, 'DD-MM-YYYY') FROM employees;

--TO_NUMBER
SELECT to_number (employee_id) FROM employees;

--CASE
SELECT
    first_name,
    last_name,
    CASE salary
        WHEN 100  THEN
            'Low'
        WHEN 5000 THEN
            'High'
        ELSE
            'medium'
    END
FROM
    employees;

--NVL
SELECT commission_pct, NVL (commission_pct, 0) FROM employees; 


--DECODE
SELECT
    employee_id,
    first_name,
    last_name,
    decode(employee_id, 101, 'Sales', 120, 'Marketing',
           130, 'Finance', 'Other') AS department_name
FROM
    employees;

--CAST
SELECT cast (salary as number) FROM employees;

--NULLIF
SELECT nullif (commission_pct, 0.5) FROM employees;
