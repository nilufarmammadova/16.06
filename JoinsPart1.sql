--Inner
SELECT
    employee_id,
    department_name,
    e.department_id
FROM
    employees e JOIN departments d ON e.department_id = d.department_id;


--Self
SELECT
    e.first_name,
    m.first_name
FROM
    employees e
JOIN employees m ON e.manager_id = m.employee_id;


--Natural
SELECT
    *
FROM
         employees
    NATURAL JOIN departments;


--Using
SELECT
    first_name,
    last_name,
    department_name,
    location_id
FROM
    employees JOIN
DEPARTMENTS USING ( DEPARTMENT_ID );
